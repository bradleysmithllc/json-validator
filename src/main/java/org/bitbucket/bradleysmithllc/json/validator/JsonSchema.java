package org.bitbucket.bradleysmithllc.json.validator;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

public class JsonSchema {
	private final JsonSchemaObjectNode jsonSchemaObjectNode;

	public JsonSchema(JsonNode root) throws JsonSchemaValidationException
	{
		if (!root.isObject())
		{
			throw new JsonSchemaValidationException("Invalid schema - not object type", "", root, null);
		}

		jsonSchemaObjectNode = new JsonSchemaObjectNode((ObjectNode) root);
	}

	public JsonSchema(String schema) throws JsonSchemaValidationException
	{
		this(JsonUtils.loadJson(schema));
	}

	JsonSchema(JsonSchemaObjectNode objectNode) throws JsonSchemaValidationException
	{
		jsonSchemaObjectNode = objectNode;
	}

	public String getId()
	{
		return jsonSchemaObjectNode.getId();
	}

	public JsonSchemaObjectNode getSchemaNode()
	{
		return jsonSchemaObjectNode;
	}
}