package org.bitbucket.bradleysmithllc.json.validator;

import java.util.HashMap;
import java.util.Map;

public class CachingSchemaResolver implements SchemaResolver {
	private final Map<String, JsonSchema> schemaCache = new HashMap<String, JsonSchema>();

	public final JsonSchema resolveByUri(String uri) throws JsonSchemaValidationException
	{
		JsonSchema jschema = schemaCache.get(uri);

		return jschema == null ? resolveByUriSub(uri) : jschema;
	}

	protected JsonSchema resolveByUriSub(String uri) throws JsonSchemaValidationException
	{
		return null;
	}

	public void registerSchemaByLocalId(String uri, JsonSchema schema)
	{
		schemaCache.put(uri, schema);
	}
}