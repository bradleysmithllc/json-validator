package org.bitbucket.bradleysmithllc.json.validator;

import java.io.*;

public class ClasspathSchemaResolver extends CachingSchemaResolver {
	private final ClassLoader loader;

	public ClasspathSchemaResolver(Object context) throws JsonSchemaValidationException {
		loader = context.getClass().getClassLoader();
	}

	public ClasspathSchemaResolver(ClassLoader cloader) throws JsonSchemaValidationException {
		loader = cloader;
	}

	public static String resolveClasspath(String url, ClassLoader loader) {
		InputStream in = loader != null ? loader.getResourceAsStream(url) : Thread.currentThread().getContextClassLoader().getResourceAsStream(url);

		if (in != null) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			int offset = 0;
			char[] buffer = new char[8192];

			StringWriter writer = new StringWriter();

			try {
				while ((offset = reader.read(buffer)) != -1) {
					writer.write(buffer, 0, offset);
				}
			} catch (IOException e) {
				throw new ResourceException("Error reading classpath stream", e);
			}

			return writer.toString();
		} else {
			throw new ResourceNotFoundException("Could not locate schema uri: " + url);
		}
	}

	public static JsonSchema resolveByUri(String uri, ClassLoader loader) throws JsonSchemaValidationException {
		String schema = resolveClasspath(uri, loader);

		JsonSchema jschema = new JsonSchema(schema);

		return jschema;
	}

	@Override
	public JsonSchema resolveByUriSub(String uri) throws JsonSchemaValidationException
	{
		return resolveByUri(uri, loader);
	}
}