package org.bitbucket.bradleysmithllc.json.validator;

public interface SchemaResolver {
	JsonSchema resolveByUri(String uri) throws JsonSchemaValidationException;
	void registerSchemaByLocalId(String uri, JsonSchema schema);
}