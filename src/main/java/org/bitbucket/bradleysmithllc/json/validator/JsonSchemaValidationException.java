package org.bitbucket.bradleysmithllc.json.validator;

import com.fasterxml.jackson.databind.JsonNode;

public class JsonSchemaValidationException extends Exception {
	private final String nodePath;
	private final JsonNode instanceNode;
	private final JsonSchemaObjectNode schemaNode;

	public JsonSchemaValidationException() {
		this.nodePath = null;
		this.instanceNode = null;
		this.schemaNode = null;
	}

	public JsonSchemaValidationException(String message, String nodePath, JsonNode instanceNode, JsonSchemaObjectNode schemaNode) {
		super(message);
		this.nodePath = nodePath;
		this.instanceNode = instanceNode;
		this.schemaNode = schemaNode;
	}

	public JsonSchemaValidationException(Throwable cause) {
		super(cause);

		this.nodePath = null;
		this.instanceNode = null;
		this.schemaNode = null;
	}

	public JsonSchemaValidationException(String message, Throwable cause) {
		super(message, cause);

		this.nodePath = null;
		this.instanceNode = null;
		this.schemaNode = null;
	}

	public String getNodePath() {
		return nodePath;
	}

	public JsonNode getInstanceNode() {
		return instanceNode;
	}

	public JsonSchemaObjectNode getSchemaNode() {
		return schemaNode;
	}

	@Override
	public String toString() {
		return "JsonSchemaValidationException{" +
			"message='" + getMessage() + "'" +
			", nodePath='" + nodePath + '\'' +
			", instanceNode=" + instanceNode +
			", schemaNode=" + schemaNode +
		'}';
	}
}