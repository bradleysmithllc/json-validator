package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public abstract class JsonSchemaBooleanTest {
	interface Getter
	{
		boolean getBoolean(JsonSchemaObjectNode node);
	}

	private final String propertyName;
	private final Getter getter;

	JsonSchemaBooleanTest(String propertyName, Getter getter)
	{
		this.propertyName = propertyName;
		this.getter = getter;
	}

	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": true }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(getter.getBoolean(schema.getSchemaNode()));

		rootNode = mapper.readValue("{ \"" + propertyName + "\": false }", JsonNode.class);

		schema = new JsonSchema(rootNode);

		Assert.assertFalse(getter.getBoolean(schema.getSchemaNode()));

		rootNode = mapper.readValue("{}", JsonNode.class);

		schema = new JsonSchema(rootNode);

		Assert.assertFalse(getter.getBoolean(schema.getSchemaNode()));
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootInvalidArrayRequired() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": [] }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootInvalidObjectRequired() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": {} }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}
}