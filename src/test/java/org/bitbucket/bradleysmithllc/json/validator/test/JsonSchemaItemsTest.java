package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class JsonSchemaItemsTest {
	@Test(expected = JsonSchemaValidationException.class)
	public void badItemsTypeString() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"items\": \"\"}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void badItemsTypeBoolean() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"items\": true}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void badItemsTypeInteger() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"items\": 0}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void badItemsTypeNumer() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"items\": 0.0}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test
	public void itemsTypeEmpty() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"items\": {}}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
		Assert.assertEquals(1, schema.getSchemaNode().getItems().size());
		Assert.assertFalse(schema.getSchemaNode().isArrayItems());
	}

	@Test
	public void itemsTypeObject() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"items\": { \"name\": \"\"}}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
		Assert.assertEquals(1, schema.getSchemaNode().getItems().size());
		Assert.assertFalse(schema.getSchemaNode().isArrayItems());
	}

	@Test
	public void itemsTypeArray() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"items\": [{ \"type\": \"array\"}]}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
		Assert.assertTrue(schema.getSchemaNode().isArrayItems());
		Assert.assertEquals(1, schema.getSchemaNode().getItems().size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_array, schema.getSchemaNode().getItems().get(0).getType().get(0));
	}

	@Test
	public void itemsTypeArrayMultiple() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"items\": [{ \"type\": \"array\"}, {\"type\": \"string\"}]}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
		Assert.assertTrue(schema.getSchemaNode().isArrayItems());
		Assert.assertEquals(2, schema.getSchemaNode().getItems().size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_array, schema.getSchemaNode().getItems().get(0).getType().get(0));
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_string, schema.getSchemaNode().getItems().get(1).getType().get(0));
	}
}