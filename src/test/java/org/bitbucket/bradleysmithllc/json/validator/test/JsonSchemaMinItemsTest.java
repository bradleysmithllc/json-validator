package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaMinItemsTest extends JsonSchemaIntegerTest {
	public JsonSchemaMinItemsTest() {
		super("minItems", new Getter() {
			public Integer getInteger(JsonSchemaObjectNode node) {
				return node.getMinItems();
			}
		}, new Integer(0), new Integer(0));
	}
}
