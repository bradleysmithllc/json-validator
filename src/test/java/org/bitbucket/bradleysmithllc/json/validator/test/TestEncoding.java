package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.junit.Assert;
import org.junit.Test;

public class TestEncoding {
	@Test
	public void test() throws JsonSchemaValidationException {
		JsonNode node = JsonUtils.loadJson("\"|\\t,\\t|\"");
		System.out.println(node.asText());
	}

	@Test
	public void prettyPrint() throws JsonSchemaValidationException
	{
		JsonNode node = JsonUtils.loadJson("{\"id\": \"Nails\", \"list\": [\"1\", \"2\"]}");

		String res = JsonUtils.printJson(node);

		Assert.assertEquals("{\n" +
				"  \"id\" : \"Nails\",\n" +
				"  \"list\" : [ \"1\", \"2\" ]\n" +
				"}", res.replace("\r", ""));
	}
}