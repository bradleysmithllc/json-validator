package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaMaximumTest extends JsonSchemaNumericTest {
	public JsonSchemaMaximumTest() {
		super("maximum", new Getter() {
			public Double getDouble(JsonSchemaObjectNode node) {
				return node.getMaximum();
			}
		});
	}
}
