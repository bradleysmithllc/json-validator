package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaFormatTest extends JsonSchemaStringTest {
	public JsonSchemaFormatTest() {
		super("format", new Getter() {
			public String getString(JsonSchemaObjectNode node) {
				return node.getFormat();
			}
		});
	}
}
