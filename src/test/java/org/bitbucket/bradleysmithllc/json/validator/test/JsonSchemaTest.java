package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class JsonSchemaTest {
	@Test
	public void rootName() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"id\": \"hardly\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
		Assert.assertEquals("hardly", schema.getId());
	}

	@Test
	public void missingRootName() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
		Assert.assertNull(schema.getId());
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void invalidRootType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("[]", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}
}