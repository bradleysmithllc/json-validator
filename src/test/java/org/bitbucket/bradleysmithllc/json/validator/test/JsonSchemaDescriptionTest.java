package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaDescriptionTest extends JsonSchemaStringTest {
	public JsonSchemaDescriptionTest() {
		super("description", new Getter() {
			public String getString(JsonSchemaObjectNode node) {
				return node.getDescription();
			}
		});
	}
}
