package org.bitbucket.bradleysmithllc.json.validator.test;

import junit.framework.Assert;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class JsonSchemaAdditionalPropertiesTest {
	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"additionalProperties\": {\"a\": {}, \"b\": {}} }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(schema.getSchemaNode().allowsAdditionalProperties());
		Assert.assertNotNull(schema.getSchemaNode().getAdditionalProperties());
	}

	@Test
	public void booleanTypeTrue() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"additionalProperties\": true }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(schema.getSchemaNode().allowsAdditionalProperties());
		Assert.assertNull(schema.getSchemaNode().getAdditionalProperties());
	}

	@Test
	public void booleanTypeFalse() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"additionalProperties\": false }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertFalse(schema.getSchemaNode().allowsAdditionalProperties());
		Assert.assertNull(schema.getSchemaNode().getAdditionalProperties());
	}

	@Test
	public void missingProperties() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(schema.getSchemaNode().allowsAdditionalProperties());
		Assert.assertNull(schema.getSchemaNode().getAdditionalProperties());
	}
}