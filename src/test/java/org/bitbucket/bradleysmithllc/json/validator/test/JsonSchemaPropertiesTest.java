package org.bitbucket.bradleysmithllc.json.validator.test;

import junit.framework.Assert;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class JsonSchemaPropertiesTest {
	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"properties\": {\"a\": {}, \"b\": {}} }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertEquals(2, schema.getSchemaNode().getPropertySchemas().size());
		Assert.assertNotNull(schema.getSchemaNode().getPropertySchemas().get("a"));
		Assert.assertNotNull(schema.getSchemaNode().getPropertySchemas().get("b"));
	}

	@Test
	public void missingProperties() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertEquals(0, schema.getSchemaNode().getPropertySchemas().size());
	}
}