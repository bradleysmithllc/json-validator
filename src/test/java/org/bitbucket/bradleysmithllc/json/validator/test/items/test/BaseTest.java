package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.json.validator.ClasspathSchemaResolver;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.bitbucket.bradleysmithllc.json.validator.JsonValidator;

import java.io.IOException;

public abstract class BaseTest {
	protected void runTest(String s_schema, String instance) throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonSchema schema = new JsonSchema(s_schema);

		JsonNode instanceNode = mapper.readValue(instance, JsonNode.class);

		JsonValidator validator = new JsonValidator(schema, new ClasspathSchemaResolver(this));

		validator.validate(instance);
	}
}