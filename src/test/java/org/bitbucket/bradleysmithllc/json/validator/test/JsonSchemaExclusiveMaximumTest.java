package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaExclusiveMaximumTest extends JsonSchemaNumericTest {
	public JsonSchemaExclusiveMaximumTest() {
		super("exclusiveMaximum", new Getter() {
			public Double getDouble(JsonSchemaObjectNode node) {
				return node.getExclusiveMaximum();
			}
		});
	}
}
