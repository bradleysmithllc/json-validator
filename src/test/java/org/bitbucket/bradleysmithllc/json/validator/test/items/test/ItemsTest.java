package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class ItemsTest extends BaseTest {
	@Test
	public void okay() throws IOException, JsonSchemaValidationException {
		runTest("{\"items\": {\"type\": \"boolean\"}}", "[true, false, true, true, false]");
		runTest("{\"items\": [{\"type\": \"boolean\"}]}", "[true, false, true, true, false]");
		runTest("{\"items\": [{\"type\": \"boolean\"}, {\"type\": \"string\"}]}", "[true, \"Hello\"]");
		runTest("{\"items\": [{\"type\": \"boolean\"}, {\"type\": \"string\"}]}", "[true, \"Hello\", true, 1]");
		runTest("{\"items\": [{\"type\": \"boolean\"}, {\"type\": \"string\"}], \"additionalItems\": {\"type\": \"integer\"}}", "[true, \"Hello\", 1, 2, 3, 4]");

		runTest("{\"items\": {\"type\": \"boolean\"}, \"maxItems\": 4, \"minItems\": 4}", "[true, false, true, true]");
		runTest("{\"items\": {\"type\": \"boolean\"}, \"uniqueItems\": true}", "[true, false]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayUniqueItems() throws IOException, JsonSchemaValidationException {
		runTest("{\"items\": {\"type\": \"boolean\"}, \"uniqueItems\": true}", "[true, false, true]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkay() throws IOException, JsonSchemaValidationException {
		runTest("{\"items\": [{\"type\": \"boolean\"}, {\"type\": \"string\"}], \"additionalItems\": false}", "[true, \"Hello\", 1, 2, 3, 4]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayMinItems() throws IOException, JsonSchemaValidationException {
		runTest("{\"items\": {\"type\": \"boolean\"}, \"minItems\": 4}", "[true, false, true]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayMaxItems() throws IOException, JsonSchemaValidationException {
		runTest("{\"items\": {\"type\": \"boolean\"}, \"maxItems\": 2}", "[true, false, true]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayEither() throws IOException, JsonSchemaValidationException {
		runTest("{\"items\": [{\"type\": \"boolean\"}, {\"type\": \"string\"}], \"additionalItems\": {\"type\": \"integer\"}}", "[true, \"Hello\", 1, 2, 3, true]");
	}
}