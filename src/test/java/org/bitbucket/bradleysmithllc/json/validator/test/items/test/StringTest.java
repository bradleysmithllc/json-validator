package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class StringTest extends BaseTest {
	@Test
	public void okayBoth() throws IOException, JsonSchemaValidationException {
		runTest("{\"maxLength\": 2, \"minLength\": 1}", "\"gg\"");
	}

	@Test
	public void okay() throws IOException, JsonSchemaValidationException {
		runTest("{\"minLength\": 7}", "\"ggffttf\"");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkay() throws IOException, JsonSchemaValidationException {
		runTest("{\"minLength\": 7}", "\"ggfftt\"");
	}

	@Test
	public void okayMax() throws IOException, JsonSchemaValidationException {
		runTest("{\"maxLength\": 2}", "\"gg\"");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayMax() throws IOException, JsonSchemaValidationException {
		runTest("{\"maxLength\": 2}", "\"ggf\"");
	}
}