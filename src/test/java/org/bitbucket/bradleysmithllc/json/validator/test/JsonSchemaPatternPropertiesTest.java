package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

public class JsonSchemaPatternPropertiesTest {
	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"patternProperties\": {\"a\": {}, \"b\": {}} }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Map<Pattern,JsonSchemaObjectNode> patternPropertySchemas = schema.getSchemaNode().getPatternPropertySchemas();
		Assert.assertEquals(2, patternPropertySchemas.size());

		Set<Pattern> keySet = patternPropertySchemas.keySet();

		for (Pattern key : keySet)
		{
			// since the set is unordered, and we can't know which will come first, we assert that each pattern
			// must match at least one of 'a' or 'b', but never both
			Assert.assertTrue(key.matcher("a").matches() || key.matcher("b").matches());
			Assert.assertFalse(key.matcher("a").matches() && key.matcher("b").matches());
		}
	}

	@Test
	public void missingProperties() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertEquals(0, schema.getSchemaNode().getPatternPropertySchemas().size());
	}
}