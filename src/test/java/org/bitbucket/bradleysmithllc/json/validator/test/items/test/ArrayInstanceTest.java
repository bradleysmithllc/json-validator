package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import junit.framework.Assert;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.bitbucket.bradleysmithllc.json.validator.JsonValidator;
import org.junit.Test;

import java.io.IOException;

public class ArrayInstanceTest {
	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{\"additionalItems\": {}}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(schema.getSchemaNode().allowsAdditionalItems());
		Assert.assertNotNull(schema.getSchemaNode().getAdditionalItems());
	}

	@Test
	public void rootObjectType2() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(schema.getSchemaNode().allowsAdditionalItems());
		Assert.assertNull(schema.getSchemaNode().getAdditionalItems());
	}

	@Test
	public void rootObjectType3() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{\"additionalItems\": true}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(schema.getSchemaNode().allowsAdditionalItems());
		Assert.assertNull(schema.getSchemaNode().getAdditionalItems());
	}

	@Test
	public void rootObjectType4() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{\"additionalItems\": false}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertFalse(schema.getSchemaNode().allowsAdditionalItems());
		Assert.assertNull(schema.getSchemaNode().getAdditionalItems());
	}

	@Test
	public void arrayProperty() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{\"enum\": [\"a\", \"b\"]}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		JsonNode instanceNode = mapper.readValue("\"a\"", JsonNode.class);

		JsonValidator validator = new JsonValidator(schema);

		validator.validate(instanceNode);
	}
}