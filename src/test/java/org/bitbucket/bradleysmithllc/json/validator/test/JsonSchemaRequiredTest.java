package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaRequiredTest extends JsonSchemaBooleanTest {
	public JsonSchemaRequiredTest() {
		super("required", new Getter() {
			public boolean getBoolean(JsonSchemaObjectNode node) {
				return node.isRequired();
			}
		});
	}
}
