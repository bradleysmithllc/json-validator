package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public abstract class JsonSchemaIntegerTest {
	interface Getter
	{
		Integer getInteger(JsonSchemaObjectNode node);
	}

	private final String propertyName;
	private final Integer defaultValue;
	private final Integer minimumValue;
	private final Getter getter;

	JsonSchemaIntegerTest(String propertyName, Getter getter, Integer defaultValue, Integer minimumValue)
	{
		this.propertyName = propertyName;
		this.getter = getter;
		this.defaultValue = defaultValue;
		this.minimumValue = minimumValue;
	}

	JsonSchemaIntegerTest(String propertyName, Getter getter)
	{
		this.propertyName = propertyName;
		this.getter = getter;
		defaultValue = null;
		this.minimumValue = null;
	}

	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": 0 }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertNotNull(getter.getInteger(schema.getSchemaNode()));
		Assert.assertEquals(0, getter.getInteger(schema.getSchemaNode()).intValue());

		rootNode = mapper.readValue("{}", JsonNode.class);

		schema = new JsonSchema(rootNode);

		if (defaultValue == null)
		{
			Assert.assertNull(getter.getInteger(schema.getSchemaNode()));
		}
		else
		{
			Assert.assertEquals(defaultValue, getter.getInteger(schema.getSchemaNode()));
		}
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootInvalidArrayRequired() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": [] }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootInvalidObjectRequired() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": {} }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootMinimumValueExceeded() throws IOException, JsonSchemaValidationException {
		if (minimumValue != null)
		{
			ObjectMapper mapper = new ObjectMapper();

			JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": " + (minimumValue.intValue() - 1) + " }", JsonNode.class);

			JsonSchema schema = new JsonSchema(rootNode);
		}
		else
		{
			throw new JsonSchemaValidationException("Just to make the tests succeed which do not have a minimal value", "", null, null);
		}
	}
}