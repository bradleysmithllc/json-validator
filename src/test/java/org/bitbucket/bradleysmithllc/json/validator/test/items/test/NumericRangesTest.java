package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class NumericRangesTest extends BaseTest {
	@Test
	public void okay() throws IOException, JsonSchemaValidationException {
		runTest("{\"maximum\": 10.1, \"minimum\": 1}", "10.1");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "10");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "9");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "8");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "7");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "6");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "5");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "4");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "3");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "2");
		runTest("{\"maximum\": 10, \"minimum\": 1}", "1");
		runTest("{\"maximum\": 10, \"minimum\": 1.1}", "1.1");

		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1}", "9.99999");
		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1}", "9");
		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1}", "8");
		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1}", "7");
		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1}", "6");
		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1}", "5");
		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1}", "4");
		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1}", "3");
		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1}", "2");
		runTest("{\"exclusiveMaximum\": 10, \"exclusiveMinimum\": 1.1}", "1.2");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayMin() throws IOException, JsonSchemaValidationException {
		runTest("{\"minimum\": 1}", "0");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayMax() throws IOException, JsonSchemaValidationException {
		runTest("{\"maximum\": 10}", "11");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayEMin() throws IOException, JsonSchemaValidationException {
		runTest("{\"exclusiveMaximum\": 10}", "10");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayEMax() throws IOException, JsonSchemaValidationException {
		runTest("{\"exclusiveMinimum\": 1}", "1");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayMin1() throws IOException, JsonSchemaValidationException {
		runTest("{\"minimum\": 1}", "0.5");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayMax2() throws IOException, JsonSchemaValidationException {
		runTest("{\"maximum\": 10}", "10.5");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayEMin3() throws IOException, JsonSchemaValidationException {
		runTest("{\"exclusiveMaximum\": 10}", "10.1");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayEMax4() throws IOException, JsonSchemaValidationException {
		runTest("{\"exclusiveMinimum\": 1}", "0.9");
	}
}