package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class ExtendsTest extends BaseTest {
	//@Test
	public void okay() throws IOException, JsonSchemaValidationException {
		runTest("{\"extends\": \"base\"}", "{}");
		runTest("{\"extends\": [\"base\", \"base2\"]}", "{}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkayWrongType() throws IOException, JsonSchemaValidationException {
		runTest("{\"extends\": {\"type\": \"boolean\"}}", "{}");
	}
}