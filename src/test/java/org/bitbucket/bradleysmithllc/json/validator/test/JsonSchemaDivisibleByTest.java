package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaDivisibleByTest extends JsonSchemaNumericTest {
	public JsonSchemaDivisibleByTest() {
		super("divisibleBy", new Getter() {
			public Double getDouble(JsonSchemaObjectNode node) {
				return node.getDivisibleBy();
			}
		});
	}
}
