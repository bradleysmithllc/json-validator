package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.bitbucket.bradleysmithllc.json.validator.JsonValidator;
import org.junit.Test;

import java.io.IOException;

public class EnumeratedValuesTest {
	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{\"enum\": [\"a\", \"b\"]}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		JsonNode instanceNode = mapper.readValue("\"a\"", JsonNode.class);

		JsonValidator validator = new JsonValidator(schema);

		validator.validate(instanceNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void missingEnum() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{\"enum\": [\"a\", \"b\"]}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		JsonNode instanceNode = mapper.readValue("\"c\"", JsonNode.class);

		JsonValidator validator = new JsonValidator(schema);

		validator.validate(instanceNode);
	}
}