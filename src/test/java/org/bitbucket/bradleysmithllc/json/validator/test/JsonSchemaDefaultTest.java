package org.bitbucket.bradleysmithllc.json.validator.test;

import junit.framework.Assert;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class JsonSchemaDefaultTest {
	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"default\": \"object\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertNotNull(schema.getSchemaNode().getDefaultValue());
		Assert.assertEquals("\"object\"", schema.getSchemaNode().getDefaultValue());
	}

	@Test
	public void rootObjectTypeObject() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"default\": {} }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertNotNull(schema.getSchemaNode().getDefaultValue());
		Assert.assertEquals("{}", schema.getSchemaNode().getDefaultValue());
	}

	@Test
	public void rootObjectTypeBoolean() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"default\": true }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertNotNull(schema.getSchemaNode().getDefaultValue());
		Assert.assertEquals("true", schema.getSchemaNode().getDefaultValue());
	}

	@Test
	public void rootObjectTypeArray() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"default\": [true, {}] }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertNotNull(schema.getSchemaNode().getDefaultValue());
		Assert.assertEquals("[true,{}]", schema.getSchemaNode().getDefaultValue());
	}
}