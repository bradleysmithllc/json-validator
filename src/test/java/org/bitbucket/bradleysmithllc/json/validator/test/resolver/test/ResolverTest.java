package org.bitbucket.bradleysmithllc.json.validator.test.resolver.test;

import org.bitbucket.bradleysmithllc.json.validator.ClasspathSchemaResolver;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.bitbucket.bradleysmithllc.json.validator.JsonValidator;
import org.junit.Test;

public class ResolverTest {
	@Test
	public void testResolver() throws JsonSchemaValidationException {
		JsonValidator validator = new JsonValidator("base.jsonSchema", new ClasspathSchemaResolver(new Object()));

		validator.validate(JsonUtils.loadJson("{\"features\": {\"database\": {\"connection-id\": \"hello\"}}}"));
	}
}