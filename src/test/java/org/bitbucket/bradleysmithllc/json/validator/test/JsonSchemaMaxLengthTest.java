package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaMaxLengthTest extends JsonSchemaIntegerTest {
	public JsonSchemaMaxLengthTest() {
		super("maxLength", new Getter() {
			public Integer getInteger(JsonSchemaObjectNode node) {
				return node.getMaxLength();
			}
		});
	}
}
