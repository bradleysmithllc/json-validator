package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaMinimumTest extends JsonSchemaNumericTest {
	public JsonSchemaMinimumTest() {
		super("minimum", new Getter() {
			public Double getDouble(JsonSchemaObjectNode node) {
				return node.getMinimum();
			}
		});
	}
}
