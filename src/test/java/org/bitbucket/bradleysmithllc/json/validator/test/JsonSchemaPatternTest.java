package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

import java.util.regex.Pattern;

public class JsonSchemaPatternTest extends JsonSchemaStringTest {
	public JsonSchemaPatternTest() {
		super("pattern", new Getter() {
			public String getString(JsonSchemaObjectNode node) {
				Pattern pattern = node.getPattern();

				if (pattern == null)
				{
					return null;
				}

				return pattern.pattern();
			}
		});
	}
}
