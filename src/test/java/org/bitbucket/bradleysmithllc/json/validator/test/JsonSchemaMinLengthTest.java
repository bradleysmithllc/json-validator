package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaMinLengthTest extends JsonSchemaIntegerTest {
	public JsonSchemaMinLengthTest() {
		super("minLength", new Getter() {
			public Integer getInteger(JsonSchemaObjectNode node) {
				return node.getMinLength();
			}
		}, new Integer(0), new Integer(0));
	}
}
