package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class PropertiesTest extends BaseTest {
	@Test
	public void okay() throws IOException, JsonSchemaValidationException {
		runTest("{\"properties\": {\"name\": {\"required\": false}}}", "{}");
		runTest("{\"properties\": {\"name\": {\"required\": true, \"type\": \"string\"}}}", "{\"name\": \"Hello\"}");
		runTest("{\"properties\": {" +
			"\"name1\": {\"required\": true, \"type\": \"string\"}," +
			"\"name2\": {\"type\": \"string\"}," +
			"\"name3\": {\"required\": true, \"type\": \"string\"}" +
			"}}",
			"{\"name1\": \"Hello\", \"name3\": \"Blubbo\"}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void missingRequired() throws IOException, JsonSchemaValidationException {
		runTest("{\"properties\": {\"name\": {\"required\": true, \"type\": \"string\"}}}", "{\"name1\": \"Hello\"}");
	}
}