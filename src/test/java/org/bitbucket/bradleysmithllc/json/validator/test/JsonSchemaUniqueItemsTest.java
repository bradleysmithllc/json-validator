package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaUniqueItemsTest extends JsonSchemaBooleanTest {
	public JsonSchemaUniqueItemsTest() {
		super("uniqueItems", new Getter() {
			public boolean getBoolean(JsonSchemaObjectNode node) {
				return node.isUniqueItems();
			}
		});
	}
}
