package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class JsonSchemaExtendsTest {
	public void extendsTypeString() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"extends\": \"\"}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void badItemsTypeBoolean() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"extends\": true}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void badItemsTypeInteger() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"extends\": 0}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void badItemsTypeNumer() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"extends\": 0.0}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void badItemsTypeObject() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"extends\": { \"name\": \"\"}}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
		Assert.assertEquals(1, schema.getSchemaNode().getItems().size());
		Assert.assertFalse(schema.getSchemaNode().isArrayItems());
	}

	@Test
	public void itemsTypeArray() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"extends\": [\"a\", \"b\"]}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
		Assert.assertEquals(2, schema.getSchemaNode().getExtends().size());
		Assert.assertEquals("a", schema.getSchemaNode().getExtends().get(0));
		Assert.assertEquals("b", schema.getSchemaNode().getExtends().get(1));
	}
}