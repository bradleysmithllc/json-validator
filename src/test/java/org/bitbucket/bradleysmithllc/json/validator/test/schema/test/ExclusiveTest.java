package org.bitbucket.bradleysmithllc.json.validator.test.schema.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.bitbucket.bradleysmithllc.json.validator.JsonValidator;
import org.junit.Test;

public class ExclusiveTest
{
	@Test
	public void exclusiveMatches() throws JsonSchemaValidationException
	{
		JsonSchema schema = new JsonSchema(JsonUtils.loadJson("{\"exclusive\": {\"items\": [{\"properties\": {\"prop1\": {\"type\": \"boolean\"}}, \"additionalProperties\": false}, {\"properties\": {\"prop2\": {\"type\": \"boolean\"}}, \"additionalProperties\": false}]}}"));

		JsonValidator val = new JsonValidator(schema);

		val.validate("{\"prop1\": true}");
		val.validate("{\"prop2\": true}");
	}

	@Test
	public void requiredExclusiveMatches() throws JsonSchemaValidationException
	{
		JsonSchema schema = new JsonSchema(JsonUtils.loadJson("{\"exclusive\": {\"items\": [{\"properties\": {\"prop1\": {\"type\": \"boolean\"}}, \"additionalProperties\": false}, {\"properties\": {\"prop2\": {\"type\": \"boolean\"}}, \"additionalProperties\": false}], \"required\": true}}"));

		JsonValidator val = new JsonValidator(schema);

		val.validate("{\"prop1\": true}");
		val.validate("{\"prop2\": true}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void requiredExclusiveMissing() throws JsonSchemaValidationException
	{
		JsonSchema schema = new JsonSchema(JsonUtils.loadJson("{\"exclusive\": {\"items\": [{\"properties\": {\"prop1\": {\"type\": \"boolean\"}}, \"additionalProperties\": false}, {\"properties\": {\"prop2\": {\"type\": \"boolean\"}}, \"additionalProperties\": false}], \"required\": true}}"));

		JsonValidator val = new JsonValidator(schema);

		val.validate("{\"prop3\": true}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void requiredExclusiveOverlap() throws JsonSchemaValidationException
	{
		JsonSchema schema = new JsonSchema(JsonUtils.loadJson("{\"exclusive\": {\"items\": [{\"properties\": {\"prop1\": {\"type\": \"boolean\"}}, \"additionalProperties\": false}, {\"properties\": {\"prop2\": {\"type\": \"boolean\"}}, \"additionalProperties\": false}], \"required\": true}}"));

		JsonValidator val = new JsonValidator(schema);

		val.validate("{\"prop1\": true, \"prop2\": true}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void exclusiveMax() throws JsonSchemaValidationException
	{
		JsonSchema schema = new JsonSchema(JsonUtils.loadJson("{\"exclusive\": {\"items\": [{}], \"minItems\": 1}}"));
	}
}