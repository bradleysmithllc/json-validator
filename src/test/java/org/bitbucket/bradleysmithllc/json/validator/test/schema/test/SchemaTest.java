package org.bitbucket.bradleysmithllc.json.validator.test.schema.test;

import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.bitbucket.bradleysmithllc.json.validator.test.items.test.BaseTest;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SchemaTest extends BaseTest
{
	@Test
	public void schemaTest() throws IOException, JsonSchemaValidationException {
		InputStream in = getClass().getResourceAsStream("/schema.jsonSchema");

		Assert.assertNotNull(in);

		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		byte [] buffer = new byte[65536];

		int i = in.read(buffer);

		while (i != -1)
		{
			bout.write(buffer, 0, i);

			i = in.read(buffer);
		}

		String str = new String(bout.toByteArray());

		runTest(str, str);
	}

	@Test
	public void idRefTest() throws IOException, JsonSchemaValidationException {
		InputStream in = getClass().getResourceAsStream("/idrefs.jsonSchema");

		Assert.assertNotNull(in);

		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		byte [] buffer = new byte[65536];

		int i = in.read(buffer);

		while (i != -1)
		{
			bout.write(buffer, 0, i);

			i = in.read(buffer);
		}

		String str = new String(bout.toByteArray());

		runTest(str, "{\"database\": {\"connection-id\": \"one\"}, \"database_ref\": {\"connection-id\": \"one\"}}");
	}

	@Test
	public void extendsRecursively() throws IOException, JsonSchemaValidationException {
		InputStream in = getClass().getResourceAsStream("/schema1.jsonSchema");

		Assert.assertNotNull(in);

		ByteArrayOutputStream bout = new ByteArrayOutputStream();

		byte [] buffer = new byte[65536];

		int i = in.read(buffer);

		while (i != -1)
		{
			bout.write(buffer, 0, i);

			i = in.read(buffer);
		}

		String str = new String(bout.toByteArray());

		runTest(str, "{\"schema1\": \"\", \"schema2\": \"\", \"schema3\": \"\"}");
	}
}