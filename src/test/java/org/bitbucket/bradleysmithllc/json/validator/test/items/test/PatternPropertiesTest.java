package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class PatternPropertiesTest extends BaseTest {
	@Test
	public void okay() throws IOException, JsonSchemaValidationException {
		runTest("{\"patternProperties\": {\"s\\\\w+\": {\"type\": \"string\"}, \"b\\\\w+\": {\"type\": \"boolean\"}}}", "{\"small\": \"Hello\", \"batter\": true, \"sari\": \"small\"}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkay() throws IOException, JsonSchemaValidationException {
		runTest("{\"patternProperties\": {\"s\\\\w+\": {\"type\": \"string\"}, \"b\\\\w+\": {\"type\": \"boolean\"}}}", "{\"small\": true}");
	}
}