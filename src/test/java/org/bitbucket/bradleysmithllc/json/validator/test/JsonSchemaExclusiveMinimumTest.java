package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaExclusiveMinimumTest extends JsonSchemaNumericTest {
	public JsonSchemaExclusiveMinimumTest() {
		super("exclusiveMinimum", new Getter() {
			public Double getDouble(JsonSchemaObjectNode node) {
				return node.getExclusiveMinimum();
			}
		});
	}
}
