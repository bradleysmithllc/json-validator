package org.bitbucket.bradleysmithllc.json.validator.test;

import junit.framework.Assert;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public abstract class JsonSchemaStringTest {
	interface Getter
	{
		String getString(JsonSchemaObjectNode node);
	}

	private final String propertyName;
	private final Getter getter;

	JsonSchemaStringTest(String propertyName, Getter getter)
	{
		this.propertyName = propertyName;
		this.getter = getter;
	}

	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": \"Hie\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertNotNull(getter.getString(schema.getSchemaNode()));
		Assert.assertEquals("Hie", getter.getString(schema.getSchemaNode()));

		rootNode = mapper.readValue("{}", JsonNode.class);

		try {
			schema = new JsonSchema(rootNode);
		} catch (JsonSchemaValidationException e) {
			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
		}

		Assert.assertNull(getter.getString(schema.getSchemaNode()));
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootInvalidArrayRequired() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": [] }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootInvalidObjectRequired() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": {} }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}
}