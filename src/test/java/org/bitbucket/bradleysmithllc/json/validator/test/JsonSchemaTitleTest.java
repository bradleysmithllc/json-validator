package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaTitleTest extends JsonSchemaStringTest {
	public JsonSchemaTitleTest() {
		super("title", new Getter() {
			public String getString(JsonSchemaObjectNode node) {
				return node.getTitle();
			}
		});
	}
}
