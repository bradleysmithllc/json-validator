package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.JsonNode;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.bitbucket.bradleysmithllc.json.validator.JsonUtils;
import org.junit.Assert;
import org.junit.Test;

public class JsonMergeTest
{
	@Test
	public void mergeObjectHierarchies() throws JsonSchemaValidationException
	{
		JsonNode nodeA = JsonUtils.loadJson("{\"a\": \"a\", \"c\": \"c\", \"d\": [1, 2, 3], \"map\": {\"a\": \"map.a\", \"b\": \"map.b\", \"c\": \"map.c\", \"map\": {\"f\": \"map.map.f\"}}}");
		JsonNode nodeB = JsonUtils.loadJson("{\"b\": \"b\", \"map\": {\"d\": \"map.d\", \"map\": {\"g\": \"map.map.g\"}}}");
		JsonNode mergeRes = JsonUtils.merge(nodeA, nodeB);

		Assert.assertNotNull(JsonUtils.query(nodeA, "a"));
		Assert.assertNull(JsonUtils.query(nodeA, "b"));
		Assert.assertNotNull(JsonUtils.query(nodeA, "c"));
		Assert.assertNotNull(JsonUtils.query(nodeA, "d"));
		Assert.assertNotNull(JsonUtils.query(nodeA, "map"));
		Assert.assertNotNull(JsonUtils.query(nodeA, "map.a"));
		Assert.assertNotNull(JsonUtils.query(nodeA, "map.b"));
		Assert.assertNotNull(JsonUtils.query(nodeA, "map.c"));
		Assert.assertNotNull(JsonUtils.query(nodeA, "map.map"));
		Assert.assertNotNull(JsonUtils.query(nodeA, "map.map.f"));
		Assert.assertEquals("a", JsonUtils.query(nodeA, "a").asText());
		Assert.assertEquals("c", JsonUtils.query(nodeA, "c").asText());

		Assert.assertEquals("map.a", JsonUtils.query(nodeA, "map.a").asText());
		Assert.assertEquals("map.b", JsonUtils.query(nodeA, "map.b").asText());
		Assert.assertEquals("map.c", JsonUtils.query(nodeA, "map.c").asText());
		Assert.assertEquals("map.map.f", JsonUtils.query(nodeA, "map.map.f").asText());

		Assert.assertNull(JsonUtils.query(nodeB, "a"));
		Assert.assertNull(JsonUtils.query(nodeB, "c"));
		Assert.assertNull(JsonUtils.query(nodeB, "d"));
		Assert.assertNotNull(JsonUtils.query(nodeB, "b"));
		Assert.assertNotNull(JsonUtils.query(nodeB, "map"));
		Assert.assertNotNull(JsonUtils.query(nodeB, "map.d"));
		Assert.assertNotNull(JsonUtils.query(nodeB, "map.map"));
		Assert.assertNotNull(JsonUtils.query(nodeB, "map.d"));
		Assert.assertNotNull(JsonUtils.query(nodeB, "map.map.g"));
		Assert.assertEquals("b", JsonUtils.query(nodeB, "b").asText());

		Assert.assertEquals("map.d", JsonUtils.query(nodeB, "map.d").asText());
		Assert.assertEquals("map.map.g", JsonUtils.query(nodeB, "map.map.g").asText());

		Assert.assertNotNull(JsonUtils.query(mergeRes, "a"));
		Assert.assertEquals("a", JsonUtils.query(mergeRes, "a").asText());
		Assert.assertEquals("a", JsonUtils.query(mergeRes, "a").asText());

		Assert.assertNotNull(JsonUtils.query(mergeRes, "b"));
		Assert.assertEquals("b", JsonUtils.query(mergeRes, "b").asText());

		Assert.assertNotNull(JsonUtils.query(mergeRes, "c"));
		Assert.assertEquals("c", JsonUtils.query(mergeRes, "c").asText());

		Assert.assertNotNull(JsonUtils.query(mergeRes, "map"));
		Assert.assertNotNull(JsonUtils.query(mergeRes, "map.a"));
		Assert.assertNotNull(JsonUtils.query(mergeRes, "map.b"));
		Assert.assertNotNull(JsonUtils.query(mergeRes, "map.c"));
		Assert.assertNotNull(JsonUtils.query(mergeRes, "map.d"));
		Assert.assertNotNull(JsonUtils.query(mergeRes, "map.map"));
		Assert.assertNotNull(JsonUtils.query(mergeRes, "map.map.f"));
		Assert.assertNotNull(JsonUtils.query(mergeRes, "map.map.g"));

		Assert.assertEquals("map.a", JsonUtils.query(mergeRes, "map.a").asText());
		Assert.assertEquals("map.b", JsonUtils.query(mergeRes, "map.b").asText());
		Assert.assertEquals("map.c", JsonUtils.query(mergeRes, "map.c").asText());
		Assert.assertEquals("map.d", JsonUtils.query(mergeRes, "map.d").asText());
		Assert.assertEquals("map.map.f", JsonUtils.query(mergeRes, "map.map.f").asText());
		Assert.assertEquals("map.map.g", JsonUtils.query(mergeRes, "map.map.g").asText());

		Assert.assertNotNull(JsonUtils.query(nodeA, "d"));
		Assert.assertEquals("[1,2,3]", JsonUtils.query(mergeRes, "d").toString());
	}

	@Test
	public void mismatchedTypesSucceed() throws JsonSchemaValidationException
	{
		JsonNode nodeA = JsonUtils.loadJson("{\"a\": \"a\"}");
		JsonNode nodeB = JsonUtils.loadJson("{\"a\": [\"b\"]}");
		JsonNode mergeRes = JsonUtils.merge(nodeA, nodeB);

		Assert.assertEquals("\"a\"", JsonUtils.query(mergeRes, "a").toString());
	}

	@Test
	public void duplicateTypesOtherThanMapSucceed() throws JsonSchemaValidationException
	{
		JsonNode nodeA = JsonUtils.loadJson("{\"a\": \"a\"}");
		JsonNode nodeB = JsonUtils.loadJson("{\"a\": \"b\"}");
		JsonNode mergeRes = JsonUtils.merge(nodeA, nodeB, JsonUtils.merge_type.right_merge);

		Assert.assertEquals("\"b\"", JsonUtils.query(mergeRes, "a").toString());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void distinctDuplicateTypesOtherThanMapFail() throws JsonSchemaValidationException
	{
		JsonNode nodeA = JsonUtils.loadJson("{\"a\": \"a\"}");
		JsonNode nodeB = JsonUtils.loadJson("{\"a\": \"b\"}");
		JsonNode mergeRes = JsonUtils.merge(nodeA, nodeB, JsonUtils.merge_type.distinct_merge);
	}

	@Test
	public void mergeLeftMerge() throws JsonSchemaValidationException
	{
		JsonNode etlTestValueObjectLeft = JsonUtils.loadJson("{\"a\": \"a\"}");
		JsonNode etlTestValueObjectRight = JsonUtils.loadJson("{\"a\": \"b\"}");

		JsonNode merge = JsonUtils.merge(etlTestValueObjectLeft, etlTestValueObjectRight, JsonUtils.merge_type.left_merge);

		Assert.assertEquals("a", JsonUtils.query(merge, "a").asText());
	}

	@Test
	public void mergeDistinctMerge() throws JsonSchemaValidationException
	{
		JsonNode etlTestValueObjectLeft = JsonUtils.loadJson("{\"a\": {\"a\": \"a\"}}");
		JsonNode etlTestValueObjectRight = JsonUtils.loadJson("{\"a\": {\"b\": \"b\"}}");

		JsonNode merge = JsonUtils.merge(etlTestValueObjectLeft, etlTestValueObjectRight, JsonUtils.merge_type.distinct_merge);

		Assert.assertEquals("a", JsonUtils.query(merge, "a.a").asText());
		Assert.assertEquals("b", JsonUtils.query(merge, "a.b").asText());
	}

	@Test
	public void mergeRightMerge() throws JsonSchemaValidationException
	{
		JsonNode etlTestValueObjectLeft = JsonUtils.loadJson("{\"a\": \"a\"}");
		JsonNode etlTestValueObjectRight = JsonUtils.loadJson("{\"a\": \"b\"}");

		JsonNode merge = JsonUtils.merge(etlTestValueObjectLeft, etlTestValueObjectRight, JsonUtils.merge_type.right_merge);

		Assert.assertEquals("b", JsonUtils.query(merge, "a").asText());
	}

	@Test
	public void directionalMergeOverridesMismatchedTypes() throws JsonSchemaValidationException
	{
		JsonNode etlTestValueObjectLeft = JsonUtils.loadJson("{\"a\": \"a\"}");
		JsonNode etlTestValueObjectRight = JsonUtils.loadJson("{\"a\": [\"b\"]}");

		JsonNode merge = JsonUtils.merge(etlTestValueObjectLeft, etlTestValueObjectRight, JsonUtils.merge_type.right_merge);

		Assert.assertEquals("[\"b\"]", JsonUtils.query(merge, "a").toString());
	}
}