package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaIdTest extends JsonSchemaStringTest {
	public JsonSchemaIdTest() {
		super("id", new Getter() {
			public String getString(JsonSchemaObjectNode node) {
				return node.getId();
			}
		});
	}
}
