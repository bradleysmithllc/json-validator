package org.bitbucket.bradleysmithllc.json.validator.test;

import junit.framework.Assert;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class JsonSchemaAdditionalItemsTest {
	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"additionalItems\": {\"a\": {}, \"b\": {}} }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(schema.getSchemaNode().allowsAdditionalItems());
		Assert.assertNotNull(schema.getSchemaNode().getAdditionalItems());
	}

	@Test
	public void booleanTypeTrue() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"additionalItems\": true }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(schema.getSchemaNode().allowsAdditionalItems());
		Assert.assertNull(schema.getSchemaNode().getAdditionalItems());
	}

	@Test
	public void booleanTypeFalse() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"additionalItems\": false }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertFalse(schema.getSchemaNode().allowsAdditionalItems());
		Assert.assertNull(schema.getSchemaNode().getAdditionalItems());
	}

	@Test
	public void missingProperties() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{}", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertTrue(schema.getSchemaNode().allowsAdditionalItems());
		Assert.assertNull(schema.getSchemaNode().getAdditionalItems());
	}
}