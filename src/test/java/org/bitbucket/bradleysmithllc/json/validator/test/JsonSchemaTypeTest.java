package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;
import java.util.List;

public class JsonSchemaTypeTest {
	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": \"object\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(1, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_object, valid_types.get(0));
	}

	@Test
	public void rootAnyType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": \"any\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(1, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_any, valid_types.get(0));
	}

	@Test
	public void rootStringType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": \"string\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(1, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_string, valid_types.get(0));
	}

	@Test
	public void rootBooleanType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": \"boolean\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(1, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_boolean, valid_types.get(0));
	}

	@Test
	public void rootIntegerType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": \"integer\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(1, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_integer, valid_types.get(0));
	}

	@Test
	public void rootNumberType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": \"number\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(1, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_number, valid_types.get(0));
	}

	@Test
	public void rootArrayType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": \"array\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(1, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_array, valid_types.get(0));
	}

	@Test
	public void rootNullType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": \"null\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(1, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_null, valid_types.get(0));
	}

	@Test
	public void rootUnionType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": [\"object\", \"array\"] }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(2, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_object, valid_types.get(0));
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_array, valid_types.get(1));
	}

	@Test
	public void rootUnionAllTypes() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": [\"object\", \"array\", \"integer\", \"number\", \"boolean\", \"null\", \"any\", \"string\"] }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		List<JsonSchemaObjectNode.valid_type> valid_types = schema.getSchemaNode().getType();
		Assert.assertNotNull(valid_types);
		Assert.assertEquals(8, valid_types.size());
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_object, valid_types.get(0));
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_array, valid_types.get(1));
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_integer, valid_types.get(2));
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_number, valid_types.get(3));
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_boolean, valid_types.get(4));
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_null, valid_types.get(5));
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_any, valid_types.get(6));
		Assert.assertEquals(JsonSchemaObjectNode.valid_type.t_string, valid_types.get(7));
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootUnionDuplicateType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": [\"null\", \"null\"] }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootBadType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"type\": \"lame\" }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}
}