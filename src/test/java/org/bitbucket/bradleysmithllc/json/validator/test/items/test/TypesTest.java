package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class TypesTest extends BaseTest {
	@Test
	public void stringType() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"string\"}", "\"Hello\"");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void stringTypeNumber() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"string\"}", "5");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void stringTypeBoolean() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"string\"}", "true");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void stringTypeArray() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"string\"}", "[true]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void stringTypeObject() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"string\"}", "{}");
	}

	@Test
	public void numberTypeDecimal() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"number\"}", "4.2");
	}

	@Test
	public void numberTypeInteger() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"number\"}", "4");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void numberTypeArray() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"number\"}", "[]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void numberTypeObject() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"number\"}", "{}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void numberTypeBoolean() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"number\"}", "true");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void integerTypeDecimal() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"integer\"}", "4.2");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void integerTypeArray() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"integer\"}", "[]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void integerTypeObject() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"integer\"}", "{}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void integerTypeBoolean() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"integer\"}", "true");
	}

	@Test
	public void integerTypeInteger() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"integer\"}", "4");
	}

	@Test
	public void arrayType() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"array\"}", "[]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void arrayTypeObject() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"array\"}", "{}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void arrayTypeString() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"array\"}", "\"\"");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void arrayTypeNumber() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"array\"}", "4.2");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void arrayTypeInteger() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"array\"}", "1");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void arrayTypeBoolean() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"array\"}", "true");
	}

	@Test
	public void objectType() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"object\"}", "{}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void objectTypeString() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"object\"}", "\"\"");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void objectTypeNumber() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"object\"}", "4.2");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void objectTypeInteger() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"object\"}", "1");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void objectTypeBoolean() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"object\"}", "true");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void objectTypeArray() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"object\"}", "[]");
	}

	@Test
	public void anyType() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"any\"}", "{}");
	}

	@Test
	public void anyTypeString() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"any\"}", "\"\"");
	}

	@Test
	public void anyTypeNumber() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"any\"}", "4.2");
	}

	@Test
	public void anyTypeInteger() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"any\"}", "1");
	}

	@Test
	public void anyTypeBoolean() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"any\"}", "true");
	}

	@Test
	public void anyTypeArray() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"any\"}", "[]");
	}

	@Test
	public void booleanType() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"boolean\"}", "true");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void booleanTypeNumber() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"boolean\"}", "5.2");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void booleanTypeInteger() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"boolean\"}", "5");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void booleanTypeString() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"boolean\"}", "\"\"");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void booleanTypeArray() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"boolean\"}", "[true]");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void booleanTypeObject() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": \"boolean\"}", "{}");
	}

	@Test
	public void unionType() throws IOException, JsonSchemaValidationException {
		runTest("{\"type\": [\"string\", \"array\"]}", "\"Hello\"");
		runTest("{\"type\": [\"string\", \"array\"]}", "[]");
		runTest("{\"type\": [\"integer\", \"boolean\", \"string\"]}", "5");
		runTest("{\"type\": [\"integer\", \"boolean\", \"string\"]}", "true");
		runTest("{\"type\": [\"integer\", \"boolean\", \"string\"]}", "false");
		runTest("{\"type\": [\"integer\", \"boolean\", \"string\"]}", "\"Hello\"");
	}
}