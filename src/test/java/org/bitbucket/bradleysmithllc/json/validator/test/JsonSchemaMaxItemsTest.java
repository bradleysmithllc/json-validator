package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaMaxItemsTest extends JsonSchemaIntegerTest {
	public JsonSchemaMaxItemsTest() {
		super("maxItems", new Getter() {
			public Integer getInteger(JsonSchemaObjectNode node) {
				return node.getMaxItems();
			}
		});
	}
}
