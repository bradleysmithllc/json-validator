package org.bitbucket.bradleysmithllc.json.validator.test.items.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public class AdditionalPropertiesTest extends BaseTest {
	@Test
	public void okay() throws IOException, JsonSchemaValidationException {
		runTest("{\"additionalProperties\": {\"type\": \"boolean\"}}", "{\"name\": true, \"name2\": false}");
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void notOkay() throws IOException, JsonSchemaValidationException {
		runTest("{\"additionalProperties\": {\"type\": \"boolean\"}}", "{\"name\": \"true\", \"name2\": false}");
	}
}