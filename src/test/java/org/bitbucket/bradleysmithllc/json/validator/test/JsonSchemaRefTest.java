package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaRefTest extends JsonSchemaStringTest {
	public JsonSchemaRefTest() {
		super("$ref", new Getter() {
			public String getString(JsonSchemaObjectNode node) {
				return node.getRef();
			}
		});
	}
}
