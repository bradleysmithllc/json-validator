package org.bitbucket.bradleysmithllc.json.validator.test;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.Assert;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchema;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;
import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaValidationException;
import org.junit.Test;

import java.io.IOException;

public abstract class JsonSchemaNumericTest {
	interface Getter
	{
		Double getDouble(JsonSchemaObjectNode node);
	}

	private final String propertyName;
	private final Integer defaultValue;
	private final Integer minimumValue;
	private final Getter getter;

	JsonSchemaNumericTest(String propertyName, Getter getter, Integer defaultValue, Integer minimumValue)
	{
		this.propertyName = propertyName;
		this.getter = getter;
		this.defaultValue = defaultValue;
		this.minimumValue = minimumValue;
	}

	JsonSchemaNumericTest(String propertyName, Getter getter)
	{
		this.propertyName = propertyName;
		this.getter = getter;
		defaultValue = null;
		this.minimumValue = null;
	}

	@Test
	public void rootObjectType() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": 0.5 }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);

		Assert.assertNotNull(getter.getDouble(schema.getSchemaNode()));
		Assert.assertEquals(0, getter.getDouble(schema.getSchemaNode()).intValue());

		rootNode = mapper.readValue("{}", JsonNode.class);

		schema = new JsonSchema(rootNode);

		if (defaultValue == null)
		{
			Assert.assertNull(getter.getDouble(schema.getSchemaNode()));
		}
		else
		{
			Assert.assertEquals(defaultValue, getter.getDouble(schema.getSchemaNode()));
		}
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootInvalidArrayRequired() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": [] }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}

	@Test(expected = JsonSchemaValidationException.class)
	public void rootInvalidObjectRequired() throws IOException, JsonSchemaValidationException {
		ObjectMapper mapper = new ObjectMapper();

		JsonNode rootNode = mapper.readValue("{ \"" + propertyName + "\": {} }", JsonNode.class);

		JsonSchema schema = new JsonSchema(rootNode);
	}
}