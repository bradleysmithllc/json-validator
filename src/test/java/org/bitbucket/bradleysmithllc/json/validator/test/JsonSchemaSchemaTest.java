package org.bitbucket.bradleysmithllc.json.validator.test;

import org.bitbucket.bradleysmithllc.json.validator.JsonSchemaObjectNode;

public class JsonSchemaSchemaTest extends JsonSchemaStringTest {
	public JsonSchemaSchemaTest() {
		super("$schema", new Getter() {
			public String getString(JsonSchemaObjectNode node) {
				return node.getSchema();
			}
		});
	}
}
